#!.pyenv/bin/python

#from gevent import monkey
# monkey.patch_all()

import os
import sys
from subprocess import Popen, PIPE
import psutil
import datetime
import time
import ujson
import json
from datetime import timedelta
import re
from copy import copy
from core.models import db
import shutil
import uuid
from core import utils
from core.config import config
from random import choice
import persian  # for persian characters
import ftfy  # for fixing sloppy text

#from hazm import Normalizer
#normalizer = Normalizer()

from elasticsearch import Elasticsearch
es = Elasticsearch([config.elasticsearch])


db.connect_to_db(mode=False)

def getDemoPages(pageCount):
    result = list([choice([i for i in range(pageCount)])
                   for k in range(int(pageCount / 15))])

    result.extend([0, 1])
    result.sort()
    return result


es_setting = {
    "settings": {
        "analysis": {
            "char_filter": {
                "zero_width_spaces": {
                    "type":       "mapping",
                    "mappings": ["\\u200C=> "]
                }
            },
            "filter": {
                "persian_stop": {
                    "type":       "stop",
                    "stopwords":  "_persian_"
                }
            },
            "analyzer": {
                "persian": {
                    "tokenizer":     "standard",
                    "char_filter": ["zero_width_spaces"],
                    "filter": [
                        "lowercase",
                        "arabic_normalization",
                        "persian_normalization",
                        "persian_stop"
                    ]
                }
            }
        }
    }
}

# curl -XPUT localhost:9200/bookestan_index1/ -d '<settings dict>'

BROKER_URL = 'redis://%s/3' % config.redis
CELERY_RESULT_BACKEND = 'redis://%s/3' % config.redis
from celery import Celery
ba = Celery('bookAgent', broker=BROKER_URL, backend=CELERY_RESULT_BACKEND)
# ca.config_from_object('bookAgentConfig')


@ba.task(name='bookAgent.simpleTest')
def simpleTest():
    print('OK')
    return 'HAPPY'


@ba.task(name='bookAgent.prepare_book')
def prepare_book(file_path, uploader_id):
    """ This function will prepare the book
    Here are te steps:
        1- Copy file to a public static folder.  original files must be save and un touched
        2- Find Pages Count
        3- Save Book basic Information
    """

    # Lets find pages Count:
    book = type('Book', (), {})
    book.extension = os.path.basename(file_path).split('.')[-1]
    book.title = os.path.basename(file_path).split('.')[0]

    ## 1 ##
    book.url = os.path.join('static', uuid.uuid4().hex[:4], uuid.uuid4().hex[
                            :12], 'book' + '.' + book.extension)
    book.public_path = os.path.join('STORAGE', 'public', book.url)
    book.public_path_dir = os.path.dirname(book.public_path)
    if not os.path.isdir(book.public_path_dir):
        os.makedirs(book.public_path_dir)
    shutil.copyfile(file_path, book.public_path)
    ## 2 ##
    book.info = utils.getPdfInfo(file_path)
    book.page_count = book.info.get('pageCount')
    book.demo_pages = getDemoPages(book.page_count)
    ## 3 ##
    cover = utils.getPdfCover(book.url)
    artworkUrl512 = utils.getImageThumbnail(cover, 512)
    artworkUrl256 = utils.getImageThumbnail(artworkUrl512, 256)
    artworkUrl100 = utils.getImageThumbnail(artworkUrl256, 100)
    artworkUrl60 = utils.getImageThumbnail(artworkUrl100, 60)
    newItem = db.BookMedia(
        uploaderId=uploader_id,
        url=book.url,
        artworkUrl=cover,
        artworkUrl512=artworkUrl512,
        artworkUrl256=artworkUrl256,
        artworkUrl100=artworkUrl100,
        artworkUrl60=artworkUrl60,
        title=book.title,
        extension=book.extension,
        version='1',
        status='uploaded',
        pageCount=book.page_count,
        mediaId=uuid.uuid4().hex[:12]
    )
    newItem.save()
    print(newItem.to_json())
    rasterize_book.delay(newItem.id)
    return newItem.id


@ba.task(name='bookAgent.rasterize_book')
def rasterize_book(book_id):
    """
    """
    print('Rasterizing %s' % book_id)
    book = db.BookMedia.objects(id=book_id).first()
    if not book:
        return

    book.status = 'processing'
    book.pages = []
    book.save()  # Save this status
    for page in range(book.pageCount):
        percent = int(((page + 1) / book.pageCount) * 100)
        page_path = utils.getPdfCover(book.url, page, page + 1)
        url1024 = utils.getImageThumbnail(page_path, 1024)
        url512 = utils.getImageThumbnail(url1024, 512)
        new_page = db.Page(
            url=page_path,
            url1024=url1024,
            url512=url512,
            number=page + 1
        )
        book.pages.append(new_page)
        book.convert_progress = percent
        book.save()
        print('%s/%s | Book: %s' % (page, book.pageCount, book.title))

    read(book.id)


@ba.task(name='bookAgent.read')
def read(book_id):
    target = db.BookMedia.objects(id=book_id).first()
    if not target:
        return

    print('Reading %s' % book_id)
    basecmd = 'nice -n 20 /usr/local/bin/pdftotext -q -l {p} -f {p} -enc UTF-8 {f} {f}.{p}.txt'
    pdffile = os.path.join('STORAGE', 'public', target.url)
    table = {}
    for each in range(target.pageCount):
        percent = int(((each + 1) / target.pageCount) * 100)
        cmd = basecmd.format(p=each + 1, f=pdffile)
        print(cmd)
        text_path = pdffile + '.' + str(each + 1) + '.txt'
        os.system(cmd)
        if os.path.isfile(text_path):
            text = open(text_path).read()
            text = utils.fix_text(text)
            #print()
            #text = normalizer.normalize(text)
#            text = ftfy.fix_text(text, normalization='NFKC')  # normalize
            # now, lets remove extra characters
            # text = text.encode('cp1256', 'ignore').decode('cp1256')
            # ##https://docs.python.org/2/library/codecs.html
            # removed wrong arabic characters
#            text = persian.arToPersianChar(text)
            #text = ftfy.fix_text(text, fix_encoding=True)
            # some ugly fixes:
            #text = text.replace('یه', 'یه ')
            #text = text.replace('ﻟـ', 'ل')
            #text = text.replace('هی', 'ه')
            db.BookMedia.objects.filter(id=book_id, pages__number=each + 1).\
                update_one(set__pages__S__text=text)
            os.remove(text_path)
        target.index_progress = percent
        target.save()
    index(book_id)
    return 'OK'


@ba.task(name='bookAgent.index')
def index(book_id):
    target = db.BookMedia.objects(id=book_id).first()
    book_id = str(book_id)
    print(target)
    if not target:
        return
    print('indexing %s' % book_id)
    for page in target.pages:
        page_id = '%s_%s' % (book_id, page.number)
        doc = {
            "book_id": book_id,
            "content": page.text,
            "page_number": page.number,
            "book_title": target.title,
            "book_isbn": target.ISBN,
            "book_tags": target.tags,
            "book_authors": ','.join(['%s %s' % (i.firstname, i.lastname) for i in target.authors]),
            "book_translators": ','.join(['%s %s' % (i.firstname, i.lastname) for i in target.translators]),
        }

        # http://192.168.99.100:9200/_search?pretty=true
        obj = es.index(index='bookestan-index8',
                       doc_type='bookpage', body=doc, id=page_id)
        print('indexed page %s' % obj)


if __name__ == '__main__':
    '''Lets ping before start'''

    argv = [
        'worker',
        '--loglevel=INFO',
        '--concurrency=4',
        '--pidfile=bookAgent.pid',
    ]
    ba.worker_main(argv)
