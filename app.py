#!/usr/bin/env python

import ujson as json
import os
import uuid
import shutil
from bottle import Bottle, request, response, static_file, auth_basic, abort, run
from core.models import db
from core import utils
from mongoengine import ValidationError
from tasks import bookAgent
import pdb
from elasticsearch import Elasticsearch
from elasticsearch.exceptions import NotFoundError, TransportError

es = Elasticsearch()

db.connect_to_db(mode=True)

application = Bottle()


__author__ = 'Farsheed Ashouri'


@application.hook('after_request')
def enable_cors():
    #response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Content-Type'] = 'application/json'


@application.error(500)
def mistake(code):
    resp = dict(status=-1, info=code.exception.message)
    return resp


@application.route('/app/<filename:path>', name='static')
def serve_app(filename):
    return static_file(filename, root='admin/app')


@application.route('/static/<filename:path>', name='static')
def serve_static(filename):
    return static_file(filename, root='STORAGE/public/static')


def check_pass(username, password):
    if username == 'why_not_to_make_some_money' and \
            password == 'just_kidding':
        return True
    else:
        return False


@application.get('/api/ping')
def ping():
    return dict(message='PONG')


@application.get('/api/listBooks')
def listBooksFirstPage():
    return listBooks(1)


@application.get('/api/listBooks/<page:int>')
@auth_basic(check_pass)
def listBooks(page):
    page_nb = page
    items_per_page = 50
    offset = (page_nb - 1) * items_per_page

    results = db.BookMedia.objects(uploaderId='bookAdmin') \
        .only('id')\
        .only('artworkUrl100').only('title').only('modified').only('convert_progress')\
        .only('index_progress').only('is_published').only('authors')\
        .only('translators').only('copyright').only('ISBN')\
        .skip(offset).limit(items_per_page)

    return results.to_json()


@application.get('/api/getFullBookInfo/<bookId>')
@auth_basic(check_pass)
def getFullBookInformation(bookId):
    target = db.BookMedia.objects(id=bookId).first()
    if not target:
        abort(404)
    return target.to_json()


@application.post('/api/updateBookInformation/<bookId>')
@auth_basic(check_pass)
def updateBookInformation(bookId):
    target = db.BookMedia.objects(id=bookId).first()
    if not target:
        abort(404)

    for obj in request.json:
        request.json[obj] = utils.fix_text(request.json[obj])
    #authors = []
    #translators = []
    #print(request.json)
    old_authors = [i.to_json() for i in target.authors]
    old_translators = [i.to_json() for i in target.translators]
    target.authors = []
    target.translators = []
    for author in request.json.get('authors'):
        if author and author not in old_authors:
            new_author =  db.Person(**author)
            if new_author.firstname or new_author.lastname:
                target.authors.append(new_author)

    for translator in request.json.get('translators'):
        if translator and translator not in old_translators:
            new_translator =  db.Person(**translator)
            if new_translator.firstname or new_translator.lastname:
                target.translators.append(new_translator)
 
    #    authors.append(db.Person(firstname=author.get('firstname').strip().title(),
    #                             lastname=author.get('lastname').strip().title()))
    #for translator in request.json.get('translators'):
    #    translators.append(db.Person(firstname=translator.get('firstname').strip().title(),
    #                                 lastname=translator.get('lastname').strip().title()))

    #request.json['authors'] = []
    #request.json['translators'] = []
    #target.authors = authors
    #target.translators = translators
    if request.json['artworkUrl'] != target.artworkUrl:
        #print('updated')
    #    target.artworkUrl = request.json['artworkUrl']
        artworkUrl = request.json['artworkUrl']
        artworkUrl512 = utils.getImageThumbnail(artworkUrl, 512)
        artworkUrl256 = utils.getImageThumbnail(artworkUrl512, 256)
        artworkUrl100 = utils.getImageThumbnail(artworkUrl256, 100)
        artworkUrl60 = utils.getImageThumbnail(artworkUrl100, 60)

        target.update(
            artworkUrl = artworkUrl,
            artworkUrl512 = artworkUrl512,
            artworkUrl256 = artworkUrl256,
            artworkUrl100 = artworkUrl100,
            artworkUrl60 = artworkUrl60,
            )


    target.update(**request.json)
    target.save()

    #bookAgent.index.delay(target.id)
    response.status = 202
    return dict(message='OK', status=0)


@application.post('/api/reIndexBook/<bookId>')
@auth_basic(check_pass)
def reIndexBook(bookId):
    target = db.BookMedia.objects(id=bookId).first()
    if not target:
        abort(404)
    bookAgent.read.delay(target.id)
    response.status = 202


@application.post('/api/reRasterBook/<bookId>')
@auth_basic(check_pass)
def reRasterBook(bookId):
    target = db.BookMedia.objects(id=bookId).first()
    if not target:
        abort(404)
    bookAgent.rasterize_book.delay(target.id)
    response.status = 202




@application.post('/api/remove/<bookId>')
@auth_basic(check_pass)
def remove(bookId):
    try:
        target = db.BookMedia.objects(id=bookId).first()
    except ValidationError:
        abort(400)
    if not target:
        abort(404)

    # ok, target is available, let's find main resource folder for it:

    """  deleting search index """
    for page in range(target.pageCount):
        try:
            es.delete(index='bookestan-index8',id ='%s_%s' % (bookId, page+1), 
                    doc_type="bookpage")
        except NotFoundError:
            pass
    mainfile = target.url

    folder = os.path.dirname(target.url)
    folder_to_delete = os.path.join('STORAGE', 'public', folder)
    target.delete()
    shutil.rmtree(folder_to_delete, True)
    return dict(info='Book Deleted')

    # ...
    # now, lets find pages


@application.get('/api/get_page_text/<book_id>/<page_number:int>')
@auth_basic(check_pass)
def get_page_text(book_id, page_number):
    target = db.BookMedia.objects.filter(id=book_id).fields('pages')
    print(target.to_json())
    #return target.to_json()



@application.post('/api/upload')
@auth_basic(check_pass)
def upload():
    """ Will get streaming files via flow.js
    ['flowTotalChunks', 'flowChunkSize', 'flowCurrentChunkSize', 'flowChunkNumber', 'flowIdentifier',
    'flowRelativePath', 'flowTotalSize', 'flowFilename']
    """
    temp_path = os.path.join('STORAGE', 'temp', request.forms.flowIdentifier)
    ''' first lets get the file'''
    with open(temp_path, 'ab') as target:
        chunk = request.files.file.file.read(
            int(request.forms.flowCurrentChunkSize))
        target.write(chunk)
    ''' now, lets save it in a good place'''

    if request.forms.flowTotalChunks == request.forms.flowChunkNumber:
        response.status = 201
        final_path = os.path.join('STORAGE', 'private', 'originals', uuid.uuid4().hex[:8],
                                  request.forms.flowRelativePath,
                                  request.forms.flowFilename.replace(' ', '-'))
        final_dir = os.path.dirname(final_path)
        if not os.path.isdir(final_dir):
            os.makedirs(final_dir)
        shutil.copyfile(temp_path, final_path)
        os.remove(temp_path)

        if request.forms.flowFilename.split('.')[-1].lower() == 'pdf':
            bookAgent.prepare_book.delay(final_path, 'bookAdmin')

        # Cover Image Uploads
        elif request.forms.flowFilename.split('.')[-1].lower() in \
                ['jpg', 'png']:
            newPublicPath = os.path.join('STORAGE', 'public', 'static',
                                         uuid.uuid4().hex[
                                             :8], uuid.uuid4().hex[:4],
                                         'image.' + request.forms.flowFilename.split('.')[-1].lower())
            final_dir = os.path.dirname(newPublicPath)
            if not os.path.isdir(final_dir):
                os.makedirs(final_dir)
            shutil.copyfile(final_path, newPublicPath)
            return dict(status=0, path=os.path.relpath(newPublicPath,
                                                       os.path.join('STORAGE', 'public')))

        return dict(status=0, message='uploaded')

    else:
        response.status = 202



def fix_all_text_problems():
    books = db.BookMedia.objects()
    for target in books:
        print('processing %s' % target.title)
        target.title = utils.fix_text(target.title)
        target.summary = utils.fix_text(target.summary)
        target.description = utils.fix_text(target.description)
        target.tags = [utils.fix_text(tag) for tag in target.tags if tag]
        target.save()


def re_index_all_books():
    books = db.BookMedia.objects()
    for book in books:
        print('Queuing %s' % book.title)
        bookAgent.read.delay(book.id)






if __name__ == '__main__':
    pass
    run(application, port=5035)
