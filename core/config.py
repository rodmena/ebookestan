
import ujson

class config:
    pass


with open('config.json') as cfile:
    #print(cfile.read())
    data = ujson.load(cfile)
    config.redis = data['redis_host']+':'+str(data['redis_port'])
    config.mongodb_host = data['mongodb_host']
    config.mongodb_port = data['mongodb_port']
    config.elasticsearch = data['elasticsearch']




