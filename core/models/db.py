
import os
import redis

from PIL import Image
from mongoengine import *
from mongoengine import signals
import base64


import datetime
from core.utils import giveUuid, getPdfCover, getImageThumbnail, getPdfInfo
from core.config import config

def connect_to_db(mode=True):
    connect('appido_books_01', host=config.mongodb_host, connect=mode)


def handler(event):
    """Signal decorator to allow use of callback functions as class decorators."""

    def decorator(fn):
        def apply(cls):
            event.connect(fn, sender=cls)
            return cls

        fn.apply = apply
        return fn

    return decorator


@handler(signals.pre_save)
def update_modified(sender, document):
    document.modified = datetime.datetime.now()


class Page(EmbeddedDocument):
    url = StringField(max_length=256)
    url1024 = StringField(max_length=256)
    url512 = StringField(max_length=256)
    number = IntField()
    text = StringField(max_length=1024 * 8)  # 8k


class Person(EmbeddedDocument):
    firstname = StringField(max_length=256)
    lastname = StringField(max_length=256)
    bio = StringField(max_length=1024)
    age = IntField()
    nationality = StringField(max_length=128)


@update_modified.apply
class Media(Document):
    uploaderFirstName = StringField(max_length=120)
    uploaderLastName = StringField(max_length=120)
    uploaderId = StringField(max_length=128, required=True)
    title = StringField(max_length=120)

    originalName = StringField(max_length=120)
    description = StringField(max_length=4096)
    copyright = StringField(max_length=128)
    mediaId = StringField(max_length=128, default=giveUuid())
    mediatype = StringField(max_length=32)  # for example: Audiobook
    contentType = StringField(max_length=64)  # for example: video/mp4
    countery = StringField(max_length=32)  # for example: IR
    currency = StringField(max_length=32)  # for example: IRR
    artworkUrl60 = StringField(max_length=256)
    artworkUrl100 = StringField(max_length=256)
    artworkUrl256 = StringField(max_length=256)
    artworkUrl512 = StringField(max_length=256)
    artworkUrl = StringField(max_length=256)
    previewUrl = StringField(max_length=256)
    publisher = StringField(max_length=256)
    status = StringField(max_length=32, choices=('uploaded', 'rasterized', 'processing',
                                                 'indexed', 'ready', 'published'))
    url = StringField(max_length=256)
    is_archived = BooleanField(default=False)
    is_public = BooleanField(default=False)
    is_published = BooleanField(default=False)
    rerender = BooleanField(default=False)
    is_password_protected = BooleanField(default=False)
    password = StringField(max_length=128)
    version = StringField(max_length=32)
    extension = StringField(max_length=32)
    price = FloatField(default=0)
    rate = IntField(default=0, choices=(0, 1, 2, 3, 4, 5))
    downloads = IntField(default=0)
    artworkSizeX = IntField()
    artworkSizeY = IntField()
    rank = IntField(default=500)
    upvotes = IntField(default=0)
    downvotes = IntField(default=0)
    owner = StringField(max_length=256)
    tags = ListField(StringField(max_length=128))
    subject = StringField(max_length=128)
    date_created = DateTimeField(default=datetime.datetime.now())
    releaseDate = DateTimeField()
    modified = DateTimeField(default=datetime.datetime.now())
    content = FileField()
    meta = {'allow_inheritance': True}


class Collection(Media):
    version = StringField(max_length=32)
    name = StringField(max_length=32)
    collectionPrice = FloatField(default=0)
    itemCount = IntField(default=0)
    items = ListField(StringField(max_length=64))


@update_modified.apply
class BookMedia(Media):
    authors = ListField(EmbeddedDocumentField(Person))
    translators = ListField(EmbeddedDocumentField(Person))
    category = StringField(max_length=32, default='book')
    ISBN = StringField(max_length=64)
    print_number = IntField()  # chappe dovvom
    print_count = IntField()  # 5000
    has_hardcopy = BooleanField(default=False)
    format = StringField(max_length=32)
    pageCount = IntField()
    demo_pages = ListField(IntField())
    summary = StringField(max_length=4096)
    pages = ListField(EmbeddedDocumentField(Page))
    convert_progress = IntField(default=0)
    index_progress = IntField(default=0)

    meta = {'indexes': [
        {'fields': [
            '$title',
            "$subject",
            "$tags"],
         'default_language': 'none',
         'weights': {
             'title': 10,
             'tags':9,
             'subject':8,
             }
        }
    ]}
