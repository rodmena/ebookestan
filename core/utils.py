
import os
from uuid import uuid4
import re
from subprocess import PIPE
import psutil
import PyPDF2 as pyPdf
import base64
import uuid
from PIL import Image
import ftfy
import unicodedata


def giveUuid():
    return str(uuid4()).replace('-', '')


def getCleanFile(filename):
    """

    :param filename:
    :return:
    """
    base = giveUuid()
    ext = filename.split('.')[-1]
    filename = os.path.join(base[1:3], base[4:6], base[8:] + '.' + ext)
    return filename


def run_command(cmd):
    """

    :param cmd:
    :return:
    """
    p = psutil.Popen(cmd.split(), stdout=PIPE, stderr=PIPE)
    _, output = p.communicate()
    result = {'stdin': _, 'stdout': output}
    return result


def getPdfCover(pdfFile, page=0, prefix='cover'):
    """

    :param pdfFile:
    :return:
    """
    if not pdfFile:
        raise ValueError('Must be a real file path to PDF file')
    prefix = str(prefix)
    basepath = os.path.join('STORAGE', 'public')
    pdfFile = os.path.join(basepath, pdfFile)
    filedir = os.path.dirname(pdfFile)
    imagename = uuid.uuid4().hex[:8]
    finalPath = os.path.join(filedir, imagename + '.png')
    cmd = 'nice -n 20 convert -density 300 -trim {i}[{p}] -quality 90 {fpath}'.\
        format(i=pdfFile, p=page, fpath=finalPath)
    run_command(cmd)
    print(cmd)
    if os.path.isfile(finalPath):
        return os.path.relpath(finalPath, os.path.join('STORAGE', 'public'))


def getImageThumbnail(filepath, size):
    if not filepath:
        #raise ValueError('Must be a real file path to Cover image file')
        return
    basepath = os.path.join('STORAGE', 'public')
    filepath = os.path.join(basepath, filepath)
    output_image_base_name = os.path.join(os.path.dirname(filepath),
                                          uuid.uuid4().hex[:8])
    result_path = '{f}.{s}.png'.format(f=output_image_base_name, s=size)
    cmd = 'nice -n 20 convert -thumbnail {s}x{s} {f} {o}'.format(f=filepath,
                                                                 s=size, o=result_path)
    run_command(cmd)
    if os.path.isfile(result_path):
        return os.path.relpath(result_path, basepath)


def getPdfInfo(pdfFilePath):
    """
    :param pdfFilePath:
    :return:
    """
    #pdfFilePath = pdfFilePath.encode('utf-8')
    fp = open(pdfFilePath, 'rb')
    try:
        pdf = pyPdf.PdfFileReader(fp)
    except PyPDF2.utils.PdfReadError:
        raise PyPDF2.utils.PdfReadError
    if pdf.isEncrypted:
        pdf.decrypt('')
    #pageCount = pdf.trailer['/Root']['/Pages']['/Count']
    pageCount = pdf.getNumPages()

    result = {
        'pageCount': pageCount
    }

    return result


def update_all_thumbnails(cover):
    #im = Image.open(os.path.join(filedir, artwork_url))
    #size = im.size
    artworkUrl512 = getImageThumbnail(cover, 512)
    print(artworkUrl512)
    artworkUrl256 = getImageThumbnail(artworkUrl512, 256)
    artworkUrl100 = getImageThumbnail(artworkUrl256, 100)
    artworkUrl60 = getImageThumbnail(artworkUrl100, 60)
    #artworkSizeX = size[0]
    #artworkSizeY = size[1]
    return dict(
        artworkUrl512=artworkUrl512,
        artworkUrl256=artworkUrl256,
        artworkUrl100=artworkUrl100,
        artworkUrl60=artworkUrl60,
        #       artworkSizeX = artworkSizeX,
        #      artworkSizeY = artworkSizeY
    )



def fix_text(text):
    if not text or not isinstance(text, (bytes, str)):
        return text
    if not isinstance(text, str):
        text=text.decode()
    fix_list = {
        1610:1740,  ## Ye (Ya)
        1577:1607,  ## he (ha)  (ex: neshaaneh)
            }

    for fix in fix_list:
        text = unicodedata.normalize('NFKC', text)
        text=re.sub(chr(fix), chr(fix_list.get(fix)), text)
    #return ftfy.fix_text(text)
    return text




if __name__ == '__main__':
    # print validate_cell('+989120228207')
    import doctest
    doctest.testmod()
