'use strict';


var getReadableFileSizeString = function (fileSizeInBytes) {
  var i = -1;
  var byteUnits = [
    ' KB',
    ' MB',
    ' GB',
    ' TB',
  ];
  do {
    fileSizeInBytes = fileSizeInBytes / 1024;
    i++;
  } while (fileSizeInBytes > 1024);
  return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
};
function pad(num, size) {
  var s = num + '';
  while (s.length < size)
  {
        s = '0' + s;
  }
  return s;

}



var app = angular.module('ngApp', ['ngRoute', 'flow', 'ui.grid']);


app.directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if(event.which === 13) {
                scope.$apply(function(){
                    scope.$eval(attrs.ngEnter || attrs.ngClick,  {'event': event});
                     }); 
          event.preventDefault();
       }
      });
            };
 });


app.config(['$routeProvider',
        function($routeProvider) {
            $routeProvider.
                when('/home', {
                    templateUrl: 'templates/home.html',
                    controller: 'homeController'
                }).
                when('/upload', {
                    templateUrl: 'templates/upload.html',
                    controller: 'uploadController'
                }).
                when('/settings', {
                    templateUrl: 'templates/settings.html',
                    controller: 'settingsController'
                }).
                when('/list', {
                    templateUrl: 'templates/list.html',
                    controller: 'listController'
                }).
                when('/list/:pageNumber', {
                    templateUrl: 'templates/list.html',
                    controller: 'listController'
                }).
                when('/search/:query', {
                    templateUrl: 'templates/search.html',
                    controller: 'searchController'
                }).
                when('/book/:bookId', {
                    templateUrl: 'templates/book.html',
                    controller: 'bookController'
                }).
                otherwise({
                    redirectTo: '/list'
                });
        }]);



app.controller('masterCtrl', function($scope, $http, $rootScope){

    console.log('Appido Books panel is loading ...');
    $rootScope.options = {};

    $scope.search = function(){
        window.location = '/admin/#/search/'+$scope.query;
    }

});

app.controller('homeController', function($scope, $http){
    console.log('Appido Books home');
});

app.controller('uploadController', function($scope, $http, $rootScope){
    $rootScope.options.current = 'upload';
    $scope.getReadableFileSizeString = getReadableFileSizeString;
    $scope.parseInt = parseInt;
});

app.controller('listController', function($scope, $http, $interval, $routeParams){
    $scope.pageNumber = $routeParams.pageNumber || 1;
    $scope.pageNumber = parseInt($scope.pageNumber);
    $scope.options = { };
    $scope.options.isLoading = false;
    console.log('Appido Books listing');


$scope.getDate = function(unix){
    return new Date(unix).toDateString();

}


$scope.getBooks = function(){

    if ($scope.options.isLoading==false){
	$scope.options.isLoading = true;
    var req = $http.get('/api/listBooks/'+$scope.pageNumber);
    req.then(function(resp){
        $scope.books = resp.data;
	$scope.options.isLoading = false;
    
    });
	}
};

$scope.getBooks();
//$scope.booksListInterval = $interval($scope.getBooks, 5000);


});


app.controller('bookController', function($scope, $http, $routeParams, $timeout){
$scope.options = { };
$scope.options.updating = false;
$scope.options.loading = false;
$scope.options.imageIsUploading = false;

$scope.subjects = [
		"سیاست",
		"شعر",
		"زندگی نامه",
		"تاریخ",
		"هنر",
		"دفاع مقدس",
		"جغرافیا",
		"اندیشه اسلام",
		"روانشناسی",
		"رایانه",
		"آموزش",
		"ورزش",
		"داستان",
		"نجوم",
		"مقالات"
]



    console.log('Book panel is loading ...')
    $scope.bookId = $routeParams.bookId;
    $scope.getFullBookInformation = function(bookId){
	$scope.options.loading = true;
        
        var req = $http.get('/api/getFullBookInfo/'+bookId);
        req.then(function(resp){
            resp.data._tags = resp.data.tags.join();
            $scope.book=resp.data;
	    	$scope.options.updating = false;
	    	$scope.options.loading = false;
        })
    

    };

$scope.getFullBookInformation($scope.bookId);


$scope.artworkUploadStarted = function(f, m, c){
    $scope.options.imageIsUploading = true;
	$scope.options.updating = true;
    $scope.$apply();
}


$scope.artworkUploadCompleted = function(f, m, c){
    $scope.result = JSON.parse(m);
    $scope.book.artworkUrl = $scope.result.path;
    $scope.book.artworkUrl512 = $scope.result.path;
    $scope.options.imageIsUploading = false;
	$scope.options.updating = false;
    $scope.$apply();
}

$scope.updateInfo = function(){

	$scope.options.updating = true;
            var tags = [];
            $scope.book._tags.split(',').forEach(function(e){tags.push(e.trim())});
            var update_info = {
                //category:$scope.book.category,
                artworkUrl:$scope.book.artworkUrl,
                title:$scope.book.title,
                price:$scope.book.price,
                ISBN:$scope.book.ISBN,
                is_published:$scope.book.is_published,
                has_hardcopy:$scope.book.has_hardcopy,
                print_number:$scope.book.print_number,
                print_count:$scope.book.print_count,
                description:$scope.book.description,
                //authorFirstName:$scope.book.authorFirstName,
                //authorLastName:$scope.book.authorLastName,
                copyright:$scope.book.copyright,
                subject:$scope.book.subject,
                summary:$scope.book.summary,
                authors:$scope.book.authors,
                translators:$scope.book.translators,
                tags:tags,
            }

            
            var req = $http.post('/api/updateBookInformation/'+$scope.bookId, update_info);
            req.then(function(resp){
                if (resp.data.status==0){
                    $scope.book = {};
                    $scope.getFullBookInformation($scope.bookId);

                    
                }
            })


}

    $scope.re_index = function(){

        if (confirm('Are you sure?')){
    
            var req = $http.post('/api/reIndexBook/'+$scope.bookId);
            req.then(function(resp){
                    alert('Book Queued for re indexing!');
                })
            }


    }


    $scope.re_raster = function(){

        if (confirm('Are you sure? This will take some time')){
    
            var req = $http.post('/api/reRasterBook/'+$scope.bookId);
            req.then(function(resp){
                    alert('Book Queued for re rasterizing!');
                })
            }


    }




    $scope.remove_book = function(){

        if (confirm('Are you sure? There is no way back')){
    
            var req = $http.post('/api/remove/'+$scope.bookId);
            req.then(function(resp){
                    alert(resp.data.info);
                    window.location='/admin/#/list';
                })
            }


    }


})


app.controller('searchController', function($scope, $http, $routeParams, $timeout){
    $scope.query = $routeParams.query;

    var req = $http.get('/bookapps/api/1.0/search?type=book_title&query='+$scope.query);  // need to change it to private search later
    req.then(function(resp){
        $scope.results = resp.data;
    })
})






