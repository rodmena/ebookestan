#!/usr/bin/env python3.5

__author = 'farsheed ashouri'
'''
This is a public API, so needs security
All APIs must exposed to /bookapps/api/v1/ for now (except PING)
'''

import ujson as json
from weppy import App, Handler, request, response, abort
from weppy.tools import service
from elasticsearch import Elasticsearch
from core.models import db
import re
es = Elasticsearch()


db.connect_to_db(mode=True)

app = App(__name__)


class DBHandler(Handler):

    def on_start(self):
        pass
        # connect to the db

    def on_success(self):
        pass
        # commit to the db

    def on_failure(self):
        pass
        # rollback the operations

    def on_end(self):
        pass
        # close the connection


class CorsHandler(Handler):

    def on_start(self):
        pass

    def on_success(self):
        response.headers['Access-Control-Allow-Origin'] = '*'
        #response.headers['Cache-Control'] = 'private'
        pass

    def on_failure(self):
        pass

    def on_end(self):
        pass


app.common_handlers = [DBHandler(), CorsHandler()]


@app.expose("/bookapps/api/ping")
@service.json
def ping():
    return dict(message='PONG')


excludes_books = '_cls', 'modified', 'description','date_created', 'is_password_protected', \
    'uploaderFirstName', 'uploaderLastName', 'uploaderId', 'is_public', 'pages',

excludes_book = 'id', '_cls', 'modified', 'date_created', 'is_password_protected', \
    'uploaderFirstName', 'uploaderLastName', 'uploaderId', 'is_public', 'pages.text',


@app.expose("/bookapps/api/1.0/getBooks")
#@service.json ## no need for this, cause to_json is prepared json format
def getBooks():
    subject = request.vars.subject
    page = request.vars.page or 0
    page_nb = int(page)
    items_per_page = 50
    offset = (page_nb - 1) * items_per_page

    if subject:
        books = db.BookMedia.objects(is_published=True, subject=subject)\
            .order_by('-modified').exclude(*excludes_books).limit(items_per_page)
    else:
        books = db.BookMedia.objects(is_published=True)\
            .order_by('-modified').exclude(*excludes_books).limit(items_per_page)
    return books.to_json()


@app.expose("/bookapps/api/1.0/getBook")
#@service.json
def getBook():
    mediaId = request.vars.bookId
    target = db.BookMedia.objects(is_published=True, id=mediaId).exclude(*excludes_book).first()
    if target:
        if target.price!=0:
            target.pages = [page for page in target.pages if page.number in target.demo_pages]
            return target.to_json()
        else:
            return raw

    else:
        abort(404)


@app.expose("/bookapps/api/1.0/search")
@service.json
def search():
    query = request.vars.query
    return json.loads(db.BookMedia.objects(is_published=True).search_text(query).exclude(*excludes_books).to_json())



@app.expose("/bookapps/api/1.0/text_search")
@service.json
def text_search():
#    '''
#    results = es.search(index='bookestan-index8', body={'query':{'match':{"content":"امام خمینی"}}})
#    '''
    query = request.vars.query
#    _type = request.vars.type
#    if not _type:
#        _type='content'
#    #else:
#    #    _type=str(_type)
#
    #query=str(query)
    if query:
        #query = query.encode('utf-8')
        match = dict(content=query)
        results = es.search(index='bookestan-index8',
                            body={'query': {'match': match}}, size=50)
        raw_results = results['hits']['hits']
        resp = [
            {
                'page_number': i['_source']['page_number'],
                'book_title':i['_source']['book_title'],
                'book_authors':i['_source']['book_authors'],
                'book_translators':i['_source']['book_translators'],
                'scrap':re.findall(r"([^.]*?{q}[^.]*\.)".format(q=query).replace('\n', ' '), i['_source']['content']),
                'book_tags':i['_source']['book_tags'],
                'book_id':i['_source']['book_id']
            }
            for i in raw_results if db.BookMedia.objects(id=i['_source']['book_id'], is_published=True)]
        return dict(results=resp)
    else:
        abort(400)


if __name__ == "__main__":
    app.run(port=5030)
